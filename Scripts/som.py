# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from time import time
from sklearn import datasets
from minisom import MiniSom
import re,os,glob,sys,getopt
from google.cloud import storage
from config_load import configloader
from tensorflow.python.lib.io import file_io

def rw(sourcefile,destfile):
			with file_io.FileIO(sourcefile, mode='r') as input_f:
				with file_io.FileIO(destfile,mode='w+') as output_f:
					output_f.write(input_f.read())

def get_all_filePaths(source_path,bucket_name=None,cloud_run=True):
    io_file_map={}
    if cloud_run==True:
        client = storage.Client()
        bucket = client.get_bucket(bucket_name)
        for i in bucket.list_blobs():
              dir_path = i.path.split('/o/')
              file_path = dir_path[-1].split('%2F')
              splitsrcpth = source_path.split('/')
              if file_path[-2]=='Log' and splitsrcpth == file_path[0:len(splitsrcpth)] and file_path[-1][-4:]=='.csv':
                  input_path = 'gs://'+bucket_name+'/'+'/'.join(file_path)
                  file_path[-2]='Results'
                  file_path[-1]='SOM'+file_path[-1]
                  output_path = 'gs://'+bucket_name+'/'+'/'.join(file_path)
                  io_file_map[input_path] = output_path
    else:	
        for root,dirs,files in os.walk(source_path):
            for filename in files:
                splitPath = root.split('/')
                if filename.endswith(('.csv')) and splitPath[-1]=='Log':
                    inputpath = root+'/'+filename
                    outputpath = '/'.join(splitPath[:-1]) + '/' + 'Results'+'/'+'SOM'+filename
                    io_file_map[inputpath]=outputpath
    return io_file_map

def apply_som(df,coordi0_len=7,coordi1_len=7,train_iter=100,sigmaval=1.0,learning_rate=0.5,max_feat=100):
    df_X = df[df.keys()[-max_feat:]]
    data = df_X.astype(np.float32).as_matrix()
    som = MiniSom(coordi0_len, coordi1_len,data.shape[1], sigma=sigmaval, learning_rate=learning_rate)
    som.random_weights_init(data)
    som.train_random(data,train_iter) # training with 100 iterations
    print("training complete...")
    df['winner'] = map(lambda x:list(som.winner(x)),data)
    df[['coordi0','coordi1']] = pd.DataFrame(df.winner.values.tolist())
    df.drop('winner',axis=1,inplace=True)
    return df

def read_in_write_out(io_file_map):
    kwargs = {'coordi0_len':conf['coordi0_len'],'coordi1_len':conf['coordi1_len'],'train_iter':conf['train_iter'],
											'sigmaval':conf['sigma'],'learning_rate':conf['learning_rate'],'max_feat':conf['max_features']}
    for filename in io_file_map.iterkeys():
        rw(filename,'temp.csv')
        df = pd.read_csv('temp.csv')
        results = apply_som(df,**kwargs)
        results.to_csv('temp.csv',index=False,header=True,columns=['Date','Message','coordi0','coordi1','Process','Controller'])
        rw('temp.csv',io_file_map[filename])

if __name__ =='__main__':
    try:
        opts,args = getopt.getopt(sys.argv[1:],'hc:j:',['config=','job-dir'])
    except getopt.GetoptError:
        print "usage code.py --config configfilepath/filename.yaml"
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print ('code.py -c configfilename')
            sys.exit(2)
        elif opt in ('-c','--config'):
            configfilename=arg
        elif opt in ('-j','--job-dir'):
            savepath = arg
    conf = configloader(configfilename)
    cloudrun = conf['cloud_run']
    read_in_write_out(get_all_filePaths(conf['source_path'],cloud_run=conf['cloud_run']))


