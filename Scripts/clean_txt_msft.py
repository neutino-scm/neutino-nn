import pandas as pd
import numpy as np
from collections import Counter
from dateutil import parser
import re, math, sys, time, os,getopt,csv
import glob, gzip
from config_load import configloader
from tensorflow.python.lib.io import file_io
from google.cloud import storage
def rw(sourcefile,destfile):
			with file_io.FileIO(sourcefile, mode='r') as input_f:
				with file_io.FileIO(destfile,mode='w+') as output_f:
					output_f.write(input_f.read())
def getall_file_path(sourcepath,destpath,bucket,cloud_run=True):
    io_file_map = {}
    in_op_file_map={}
    destindex = 0
    if cloud_run==True:
        bucket_name = bucket
        client = storage.Client()
        bucket = client.get_bucket(bucket_name)
        splitsrcpth = sourcepath.split('/')
        for i in bucket.list_blobs():
            buck_dir_path = i.path.split('/o/')
            file_path = buck_dir_path[-1].split('%2F')
            if splitsrcpth == file_path[:-1] and file_path[-1].endswith('.csv'):
                input_file = "gs://"+bucket+'/'+'/'.join(file_path)
                output_file = "gs://"+bucket + '/'+destpath+'/Microsoft/Log/messages'+str(destindex)+'.csv'
                in_op_file_map[file_path[-1]]='messages'+str(destindex)+'.csv'
                destindex+=1
                io_file_map[input_file] = output_file
        with open('filnamemap.csv', 'wb') as csv_file:
                    writer = csv.writer(csv_file)
                    for key, value in in_op_file_map.items():
                        writer.writerow([key, value])
        rw('filnamemap.csv',"gs://"+bucket + '/'+destpath+'/Microsoft/filenamemap.csv')
    else: 
        allfiles = glob.glob(sourcepath+'/*.csv')
        for filename in allfiles:
            destpath = destpath+'/Microsoft/Log'
            if not os.path.exists(destpath): os.makedirs(destpath)
            io_file_map[filename] = destpath+'/messages'+str(destindex)+'.csv'
            destindex+=1
    return io_file_map
def cleanup_for_microsoft(io_file_map,cloudrun=True):
    if cloudrun == True:
        for filename in io_file_map.iterkeys():
            rw(filename,'temp.csv')
            df = pd.read_csv('temp.csv')
            df.rename(columns={'File':'Controller'})
            df['Process'] = df.Message.map(lambda x:x.split(' ')[0])
            df.to_csv('temp.csv',header=True,index=False,columns=['Process','Controller','Date','Message'])
            rw('temp.csv',io_file_map[filename])
    else:
        for filename in io_file_map.iterkeys():
            df = pd.read_csv(filename)
            df.rename(columns={'File':'Controller'})
            df['Process'] = df.Message.map(lambda x:x.split(' ')[0])
            df.to_csv(io_file_map[filename],header=True,index=False,columns=['Process','Controller','Date','Message'])

if __name__ =='__main__':
    try:
        opts,args = getopt.getopt(sys.argv[1:],'hc:j:',['config=','job-dir'])
    except getopt.GetoptError:
        print "usage code.py --config configfilepath/filename.yaml"
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print ('code.py -c configfilename')
            sys.exit(2)
        elif opt in ('-c','--config'):
            configfilename=arg
        elif opt in ('-j','--job-dir'):
            savepath = arg
    
    conf = configloader(configfilename)
    sourcepath = conf['sourcepath']
    destpath = conf['destpath']
    bucket = conf['bucket']
    cleanup_for_microsoft(getall_file_path(sourcepath,destpath,bucket))

