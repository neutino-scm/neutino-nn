import sys, time
from collections import Counter
import pandas as pd
import numpy as np
import datetime
import os
from google.cloud import storage
import pickle
from tensorflow.python.lib.io import file_io
from config_load import configloader
import getopt

def rw(sourcefile,destfile):
			with file_io.FileIO(sourcefile, mode='r') as input_f:
				with file_io.FileIO(destfile,mode='w+') as output_f:
					output_f.write(input_f.read())

def median_deviation_anomaly_detection(df, colname):        
    cTime= df.counts.values # column 'counts' is the number count of anomalies at each time interval
    med= np.median(cTime)
    mad= conf['madvalue']* np.median(np.abs(cTime - med))
    if mad < 1.0: mad= 1.0          
    df[colname]= df.counts.map(lambda x: np.abs(x-med)/mad)
    return df

def mean_deviation_anomaly_detection(df, colname):        
    cTime= df.counts.values # column 'counts' is the number count of anomalies at each time interval
    avgCount= np.mean(cTime)
    stdCount= np.std(cTime)
    if stdCount < 1.0: stdCount= 1.0          
    df[colname]= df.counts.map(lambda x: np.abs(x-avgCount)/stdCount)
    return df


def get_all_filePaths(source_path,bucket_name='panztest',cloud_run=True):
    	io_file_map={}
	if cloud_run==True:
         client = storage.Client()
         bucket = client.get_bucket(bucket_name)
         splitsrcpth = source_path.split('/')
         for i in bucket.list_blobs():
             dir_path = i.path.split('/o/')
             file_path = dir_path[-1].split('%2F')
             cndt1 = file_path[-2]=='Results' and splitsrcpth==file_path[0:len(splitsrcpth)] and file_path[-1][-4:]=='.pkl'
             if cndt1:
                 input_path = 'gs://'+bucket_name+'/'+'/'.join(file_path)
                 message_file_path = 'gs://'+bucket_name+'/'+'/'.join(file_path[:-2])+'/Log/'+file_path[-1].split('_')[0]+'.csv'
                 file_path[-1]='Stats_'+file_path[-1].split('_')[0]+'.csv'
                 output_path = 'gs://'+bucket_name+'/'+'/'.join(file_path)
                 io_file_map[input_path] = [output_path,message_file_path]
	else:
         for root,dirs,files in os.walk(source_path):
             for filename in files:
                   splitPath = root.split('/')
                   cndt1 = splitPath[-1]=='Results' and filename[-4:]=='.pkl'
                   input_path = root+'/'+filename
                   if cndt1:
                       output_path = root+'/Stats_'+filename.split('_')[0]+'.csv'
                       message_file_path = '/'.join(splitPath[:-1])+'/Log/'+ filename.split('_')[0]+'.csv'
                       io_file_map[input_path]=[output_path,message_file_path]
    	return io_file_map 

def outlier_detection(df, path_to_save, *dets):
    
    aCols= ['Freq_Anom_' + str(i) for i in range(len(dets))] # create column names for each type of anomaly detector    
    #plots= [plt.subplots(len(aCols), figsize=(20, 20)) for i in range(num_clusters)]
    anoms= pd.DataFrame()

    # Loop over clusters, generate anomaly scores, and merge into one dataframe
    for key, group in df.groupby(['idx']):
        group['IndexDF']= group.index
        group['DateTime']= group.Date.map(lambda x:x//900)
        gtmp= group.groupby(['DateTime'],as_index=False).size().rename('counts').reset_index()
        
        for i in range(len(aCols)): gtmp= dets[i](gtmp, aCols[i]) # Generate Anomaly Scores for each Anomaly Detection       
        anoms= pd.concat([anoms,pd.merge(group, gtmp, on=['DateTime'], how='inner')])
    df_anoms= pd.merge(df, anoms, how='inner', left_index=True, right_on=['IndexDF'], suffixes=('','_y'))

    # Test that merge worked
    t1= [x for x in df_anoms.columns.values if '_y' in x]
    t2= [x[:-2] for x in t1]
    if ~(df_anoms.ix[:,t1].as_matrix()==df_anoms.ix[:,t2].as_matrix()).all(): 
        print('Error! on Merge')
        sys.exit()
    # Generate a csv file with anomaly scores for each type of anomaly detector
    df_anoms.drop(t1+['IndexDF'], inplace=True, axis=1)
    df_anoms.to_csv('temp1.csv', index=False)
    rw('temp1.csv',path_to_save)

def apply_stats_tofiles(io_file_map):
    for filename in io_file_map.iterkeys():
        rw(filename,'temp1.pkl')
        msg_file = io_file_map[filename][1]
        rw(msg_file,'temp1.csv')
        data= pickle.load(open("temp1.pkl","rb"))['q']
        df= pd.read_csv('temp1.csv')
        df['p_idx']= np.max(data, axis=1) 
        df['idx']= np.argmax(data, axis=1)
        outlier_detection(df,io_file_map[filename][0],[median_deviation_anomaly_detection,mean_deviation_anomaly_detection])
        
if __name__ =='__main__':
    try:
        opts,args = getopt.getopt(sys.argv[1:],'hc:j:',['config=','job-dir'])
    except getopt.GetoptError:
        print "usage code.py --config configfilepath/filename.yaml"
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print ('code.py -c configfilename')
            sys.exit(2)
        elif opt in ('-c','--config'):
            configfilename=arg
        elif opt in ('-j','--job-dir'):
            savepath = arg
    conf = configloader(configfilename)
    cloudop = conf['cloud_run']
    apply_stats_tofiles(get_all_filePaths(conf['source_path'],conf['bucket_name'],cloud_run=cloudop))