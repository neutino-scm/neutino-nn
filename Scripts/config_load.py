import yaml
from tensorflow.python.lib.io import file_io

def configloader(config = "gs://panzuracleancode/Data/Input/conf_Clean_txt.yaml"):
 with file_io.FileIO(config,"r") as f:
    config = yaml.load(f)
 return config

#def configloader(path):
#	with open(path,"r") as f:
#	    return yaml.load(f)
