# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from minisom import MiniSom
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
import re,os,glob,sys,getopt
from google.cloud import storage
from config_load import configloader
from tensorflow.python.lib.io import file_io
from datetime import datetime
if (sys.version[0] == 2):
    import cPickle as pickle
else:
    import pickle
import json
def rw(sourcefile,destfile):
			with file_io.FileIO(sourcefile, mode='r') as input_f:
				with file_io.FileIO(destfile,mode='w+') as output_f:
					output_f.write(input_f.read())
def append_record(df,filename,offset,savefile):
    with open(savefile,"a+") as f:
     for index,process in enumerate(df.Processs):
         try:
             dateandtime = datetime.fromtimestamp(df.Date[index]).strftime('%Y-%m-%d %H:%M:%S')
         except TypeError:
            dateandtime = df.Date[index]
         
         rowrec = {'DeviceName':df.Controller[index],'Detectedtime':datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S"),"Date":dateandtime,
                          "Message":df.Message[index],"Process":df.Process[index],"incidentNo":str(index+offset),"FileName":filename}
         json.dump(rowrec,f)
         f.write(os.linesep)

def getfiles_conv_json(source_path,bucket_name,cloud_run=True):
	if cloud_run==True:
           client = storage.Client()
           splitsrcpth = source_path.split('/')
           bucket = client.get_bucket(bucket_name)
           offset1=0
           offset2=0
           for i in bucket.list_blobs():
                 dir_path = i.path.split('/o/')
                 file_path = dir_path[-1].split('%2F')
                 cndt1 = file_path[-2]=='Results' and file_path[-1][0:15]=='DBSCAN_messages' and file_path[-1][-4:]=='.csv' and splitsrcpth==file_path[0:len(splitsrcpth)]
                 cndt2 = file_path[-2]=='Results' and file_path[-1][0:10]=='DBSCAN_SOM' and file_path[-1][-4:]=='.csv' and splitsrcpth==file_path[0:len(splitsrcpth)]
                 input_path = 'gs://'+bucket_name+'/'+'/'.join(file_path)
                 if  cndt1:
                      rw(input_path,'temp.csv')
                      df = pd.read_csv('temp.csv')
                      append_record(df,file_path[-1].split('_')[1],offset1,'temp.json')
                      offset1 = offset1 + df.shape[0]
                      savepath = file_path[:-2]
                 elif cndt2:
                      rw(input_path,'temp1.csv')
                      df = pd.read_csv('temp1.csv')
                      append_record(df,file_path[-1].split('_')[2],offset2,'temp1.json')
                      offset2 = offset2 + df.shape[0]                    
                      savepath = file_path[:-2]
           pathtoSave = "gs://"+'/'.join(savepath)
           if offset1 !=0: rw('temp1.json',pathtoSave+"/AnomalyDetailsSOM.json")
           if offset2 !=0: rw('temp.json',pathtoSave+"/AnomalyDetailsDEC.json")
	else:
         for root,dirs,files in os.walk(source_path):
              for filename in files:
                   splitPath = root.split('/')
                   cndt1 = splitPath[-1]=='Results' and filename[0:12]=='DBSCAN_messages' and filename[-4:]=='.csv'
                   cndt2 = splitPath[-1]=='Results' and filename[0:8]=='DBSCAN_SOM' and filename[-4:]=='.csv'
                   input_path = root+'/'+filename
                   offset1=0
                   offset2=0
                   if cndt1:
                      pathtoSave = '/'.join(splitPath[:-1])+'/AnomalyDetailsDEC.json'
                      df = pd.read_csv(input_path)
                      append_record(df,filename.split('_')[1],offset1,pathtoSave)
                      offset1 = offset1 + df.shape[0]
                   elif cndt2:
                      pathtoSave = '/'.join(splitPath[:-1])+'/AnomlayDetailSOM.json'
                      df = pd.read_csv(input_path)
                      append_record(df,filename.split('_')[2],offset1,pathtoSave)
                      offset2 = offset2 + df.shape[0]
if __name__ =='__main__':
    try:
        opts,args = getopt.getopt(sys.argv[1:],'hc:j:',['config=','job-dir'])
    except getopt.GetoptError:
        print "usage code.py --config configfilepath/filename.yaml"
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print ('code.py -c configfilename')
            sys.exit(2)
        elif opt in ('-c','--config'):
            configfilename=arg
        elif opt in ('-j','--job-dir'):
            savepath = arg
    conf = configloader(configfilename)
    cloudop = conf['cloud_run']
    srcpth = conf['srcpth']
    getfiles_conv_json(srcpth,cloud_run=cloudop)           
      