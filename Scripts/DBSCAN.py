import pandas as pd
import numpy as np
from minisom import MiniSom
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
import re,os,glob,sys,getopt
from google.cloud import storage
from config_load import configloader
from tensorflow.python.lib.io import file_io
if (sys.version[0] == 2):
    import cPickle as pickle
else:
    import pickle
    
def rw(sourcefile,destfile):
			with file_io.FileIO(sourcefile, mode='r') as input_f:
				with file_io.FileIO(destfile,mode='w+') as output_f:
					output_f.write(input_f.read())
def get_all_filePaths(source_path,bucket_name='panztest',cloud_run=True):
    	io_file_map={}
	if cloud_run==True:
         client = storage.Client()
         bucket = client.get_bucket(bucket_name)
         splitsrcpth = source_path.split('/')
         for i in bucket.list_blobs():
             dir_path = i.path.split('/o/')
             file_path = dir_path[-1].split('%2F')
             cndt1 = file_path[-2]=='Results' and splitsrcpth==file_path[0:len(splitsrcpth)] and file_path[-1][-4:]=='.csv'
             cndt2 = file_path[-2]=='Results' and splitsrcpth==file_path[0:len(splitsrcpth)] and file_path[-1][-4:]=='.pkl'
             if  cndt1:
                 input_path = 'gs://'+bucket_name+'/'+'/'.join(file_path)
                 file_path[-1]='DBSCAN_'+file_path[-1]
                 output_path = 'gs://'+bucket_name+'/'+'/'.join(file_path)
                 io_file_map[input_path] = [output_path]
             elif cndt2:
                 input_path = 'gs://'+bucket_name+'/'+'/'.join(file_path)
                 message_file_path = 'gs://'+bucket_name+'/'+'/'.join(file_path[:-2])+'/Log/'+file_path[-1].split('_')[0]+'.csv'
                 file_path[-1]='DBSCAN_'+file_path[-1][:-4]+'.csv'
                 output_path = 'gs://'+bucket_name+'/'+'/'.join(file_path)
                 io_file_map[input_path] = [output_path,message_file_path]
	else:
         for root,dirs,files in os.walk(source_path):
             for filename in files:
                   splitPath = root.split('/')
                   cndt1 = splitPath[-1]=='Results' and filename[-4:]=='.csv'
                   cndt2 = splitPath[-1]=='Results' and filename[-4:]=='.pkl'
                   input_path = root+'/'+filename
                   output_path = root+'/DBSCAN_'+filename[:-4]+'.csv'
                   if cndt1:
                       io_file_map[input_path] = [output_path]
                   elif cndt2:
                       message_file_path = '/'.join(splitPath[:-1])+'/Log/'+ filename.split('_')[0]+'.csv'
                       io_file_map[input_path]=[output_path,message_file_path]
    	return io_file_map

def apply_dbscan(x,minSample=100,radius=0.1):
	X = StandardScaler().fit_transform(x)
	db=DBSCAN(eps=radius,min_samples=minSample).fit(X)
	return db.labels_
	
def readin_applyDB_writeout(io_file_map):
    for file in io_file_map.iterkeys():
		if len(io_file_map[file])==1:
			rw(file,'tmp.csv')
			df = pd.read_csv('tmp.csv')
		elif len(io_file_map[file])==2:
                 rw(io_file_map[file][1],'tmp.csv')
                 df = pd.read_csv('tmp.csv')
                 rw(file,'tmp1.pkl')
                 with open('tmp1.pkl','r') as f:
				dec_data = pickle.load(f)
                 df['coordi0'] = dec_data['z_2d'].T[0]
                 df['coordi1'] = dec_data['z_2d'].T[1]
                 df['DEClabel'] = dec_data['q'].argmax(1)
		x = df.as_matrix(columns=['coordi0','coordi1'])
		batch_size = conf['batch_size']
		index = 0
		labels = []
		while index*batch_size <= x.shape[0]:
			if x[index*batch_size:(index+1)*batch_size].shape[0]==batch_size:
				X = x[index*batch_size:(index+1)*batch_size]
				batch_labels = apply_dbscan(X,minSample=conf['min_samples'],radius=conf['eps'])
			else :
				X = x[index*batch_size::]
				batch_labels = apply_dbscan(X,minSample=conf['min_samples'],radius=conf['eps'])
			index+=1
			labels.extend(batch_labels)
			print(len(labels))
		print df.shape
		df['DBlabels'] = labels
		print len(labels)
		outlierDF = df[df['DBlabels']==-1]
		outlierDF.to_csv('temp.csv',columns=['Date','Message','Process','DBlabels','coordi0','coordi1','Controller'],index=False,header=True)
		rw('temp.csv',io_file_map[file][0])
		
if __name__ =='__main__':
    try:
        opts,args = getopt.getopt(sys.argv[1:],'hc:j:',['config=','job-dir'])
    except getopt.GetoptError:
        print "usage code.py --config configfilepath/filename.yaml"
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print ('code.py -c configfilename')
            sys.exit(2)
        elif opt in ('-c','--config'):
            configfilename = arg
        elif opt in ('-j','--job-dir'):
            savepath = arg
    conf = configloader(configfilename)
    readin_applyDB_writeout(get_all_filePaths(conf['source_path'],bucket_name=conf['bucket_name'],cloud_run=conf['cloud_run']))

    
    
    
    
    
            