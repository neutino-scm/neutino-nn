import pandas as pd
import numpy as np
from collections import Counter
from dateutil import parser
import re, math, sys, time, os,getopt
import glob, gzip
from config_load import configloader
from tensorflow.python.lib.io import file_io
from google.cloud import storage

def get_all_filePaths(sourcepath,dest_dir,cloud_run=False):
    io_file_map = {}
    print sourcepath,dest_dir
    if cloud_run==True:
        bucket_name = sourcepath
        client = storage.Client()
        bucket = client.get_bucket(bucket_name)
        for i in bucket.list_blobs():
            buck_dir_path = i.path.split('/o/')
            dir_path = buck_dir_path[-1].split('%2F')
            filename_split = dir_path[-1].split('.')
            if dir_path[-2]=='Log' and filename_split[-1]=='gz' and filename_split[-3]=='messages':
                input_path = 'gs://'+bucket_name+'/'+'/'.join(dir_path)
                dir_path[1] = dest_dir
                dir_path[-1] = filename_split[-3]+filename_split[-2]+'.csv'
                output_path = 'gs://'+bucket_name+'/'+'/'.join(dir_path)
                io_file_map[input_path]=output_path
    else:
          for root,dirs,files in os.walk(sourcepath):
                  for filename in files:
                       if filename.endswith(('.gz')):
                            pathSplit = root.split('/')
                            if pathSplit[-1] == 'Log': 
                                 CustomerName = pathSplit[-3]
                                 NodeName = pathSplit[-2]
                                 MessageName = ''.join(filename.split('.')[:-1])
                                 io_file_map[os.path.join(root,filename)] = os.path.join(dest_dir,CustomerName,NodeName,'Log',MessageName)+'.csv'
                                 destPath = os.path.join(dest_dir,CustomerName,NodeName,'Log')
                                 if not os.path.exists(destPath) :os.makedirs(destPath)
    return io_file_map
 
def clean_file(file_path):
                 with gzip.open(file_path,"rb") as f:
                     cplfile = f.readlines()
                 linesofLogsinFile = [re.sub(b'\s+', b' ', line).strip().split(b' ') for line in cplfile]
                 linesofLogsinFile = [[row for row in nF] for nF in linesofLogsinFile]
                 tim_proc_message = [[parser.parse(b' '.join(d[0:3])), d[3],d[4],' '.join(d[5:])] for d in linesofLogsinFile]
                 df = pd.DataFrame(tim_proc_message,columns=['Date','Controller','Process','Message'])
                 return df
       
def rw(sourcefile,destfile):
			with file_io.FileIO(sourcefile, mode='r') as input_f:
				with file_io.FileIO(destfile,mode='w+') as output_f:
					output_f.write(input_f.read())
     
def read_applyclean_write(io_file_map,cloud_run=True):
    for in_file in io_file_map.iterkeys():
                 rw(in_file,'temp.gz')
                 df = clean_file('temp.gz')
                 df.to_csv('temporary.csv',index=False)
                 rw('temporary.csv',io_file_map[in_file])

if __name__ =='__main__':
    try:
        opts,args = getopt.getopt(sys.argv[1:],'hc:j:',['config=','job-dir'])
    except getopt.GetoptError:
        print "usage code.py --config configfilepath/filename.yaml"
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print ('code.py -c configfilename')
            sys.exit(2)
        elif opt in ('-c','--config'):
            configfilename=arg
        elif opt in ('-j','--job-dir'):
            savepath = arg
    conf = configloader(configfilename)
    cloudop = conf['cloud_run']
    read_applyclean_write(get_all_filePaths(conf['sourcepath'],conf['dest_dir'],cloud_run=cloudop),cloud_run=cloudop)
