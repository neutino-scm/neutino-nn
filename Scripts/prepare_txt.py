import pandas as pd
import numpy as np
from collections import Counter
from dateutil import parser
import re, math, sys, time, os, nltk, glob,getopt
from nltk.corpus import stopwords
from string import punctuation
from tensorflow.python.lib.io import file_io
from google.cloud import storage
nltk.download('stopwords')
nltk.download('punkt')
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from config_load import configloader

def rw(sourcepath,destpath):
			with file_io.FileIO(sourcepath, mode='r') as input_f:
				with file_io.FileIO(destpath,mode='w+') as output_f:
					output_f.write(input_f.read())

def cleanup(file_content):
    english_stops= set(stopwords.words('english'))
    tokens= nltk.word_tokenize(file_content)         
    tokensMax= map(lambda word: max(re.split('[^a-zA-Z]', word), key=len), tokens)
    tokensMax= [word for word in tokensMax if len(word) > 1 and word not in english_stops and not word.isdigit()]
    return str(' '.join(tokensMax))

def get_all_filePaths(sourcepath,bucketname=None,cloud_run=True):
    io_file_map = {}
    if cloud_run==True:
        bucket_name = bucketname
        client = storage.Client()
        bucket = client.get_bucket(bucket_name)
        for i in bucket.list_blobs():
            buck_dir_path = i.path.split('/o/')
            splitsrcpth = sourcepath.split('/')
            file_path = buck_dir_path[-1].split('%2F')
            if file_path[-2]=='Log' and splitsrcpth == file_path[0:len(splitsrcpth)]:
                input_path = 'gs://'+bucket_name+'/'+'/'.join(file_path)
                file_path[-1] = '_'+file_path[-1]
                output_path = 'gs://'+bucket_name+'/'+'/'.join(file_path)
                io_file_map[input_path]=output_path
    else:
        for root,dirs,files in os.walk(sourcepath):
            for filename in files:
                if root.split('/')[-1] == 'Log'and filename[0:8]=='messages' and filename[-4:]=='.csv':
					SourcefilePath = os.path.join(root,filename)
					DestfilePath = os.path.join(root,'_'+filename)
					io_file_map[SourcefilePath]= DestfilePath
    return io_file_map

def text2num_conv(df,max_feat=100):	
        df.Message= df.Message.map(lambda x: cleanup(str(x).lower()))
        data = df.Message
        data = data.tolist()
        X = CountVectorizer(dtype=np.float64, max_features= max_feat).fit_transform(data)
        X = TfidfTransformer(norm='l2', sublinear_tf=True).fit_transform(X)
        X = np.asarray(X.todense())*np.sqrt(X.shape[1])
        dataX = pd.DataFrame(X,columns=range(int(max_feat)))
        df.Date = pd.DatetimeIndex(df.Date).astype(np.int64)//10**9
        dataX.insert(0, 'Controller', df.Controller)
        dataX.insert(1,'Date',df.Date)
        dataX.insert(2,'Process',df.Process)
        dataX.insert(3,'Message',df.Message)
        return dataX

def read_prepare_write(io_file_map,cloud_run=True,max_feat=100):
    for filepath in io_file_map.iterkeys():
        rw(filepath,'tempfile.csv')
        df = pd.read_csv('tempfile.csv')
        data = text2num_conv(df,max_feat= max_feat)
        data.to_csv('temp.csv',index=False,header=True)
        rw('temp.csv',io_file_map[filepath])                               

if __name__ =='__main__':
    try:
        opts,args = getopt.getopt(sys.argv[1:],'hc:j:',['config=','job-dir'])
    except getopt.GetoptError:
        print "usage code.py --config configfilepath/filename.yaml"
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print ('code.py -c configfilename')
            sys.exit(2)
        elif opt in ('-c','--config'):
            configfilename=arg
        elif opt in ('-j','--job-dir'):
            savepath = arg
    conf = configloader(configfilename)
    cloudop = conf['cloud_run']
    #dataset = conf['dataset']
    maxfeat = conf['max_features']
    bucket_name = conf['bucket_name']
    read_prepare_write(get_all_filePaths(conf['sourcepath'],bucketname=bucket_name,cloud_run=cloudop),cloud_run=cloudop,max_feat= maxfeat)

        